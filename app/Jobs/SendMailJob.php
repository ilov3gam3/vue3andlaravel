<?php

namespace App\Jobs;

use App\Mail\ActiveMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $mail_to;
    public $data;
    public function __construct($mail_to, $data)
    {
        $this->mail_to = $mail_to;
        $this->data = $data;
    }

    public function handle()
    {
        Mail::to($this->mail_to)->send(new ActiveMail($this->data));
    }
}
