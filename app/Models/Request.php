<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;
    protected $fillable = [
        'path',
        'ip',
        'user_agent',
        'method',
        'parameter',
        'login_as'
    ];
}
