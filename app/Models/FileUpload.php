<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    use HasFactory;
    protected $fillable =[
        'path',
        'size',
        'user_id',
        'file_type_id',
        'is_local'
    ];
}
