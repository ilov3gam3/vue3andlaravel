<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $fillable =[
        'message',
        'user_sender',
        'user_receiver'
    ];
    protected $casts = [
        'user_sender' => 'integer',
        'user_receiver' => 'integer',
    ];
}
