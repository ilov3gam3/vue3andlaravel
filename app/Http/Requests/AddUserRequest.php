<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:50|unique:users,username',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6',
        ];
    }
    public function messages()
    {
        return [
            'required'      => ':attribute không được để trống',
            'min' => ':attribute phải dài hơn 6 chữ số',
            'max' => ':attribute không được dài hơn 50 kí tự',
        ];
    }
    public function attributes()
    {
        return [
            'username' => 'Tên',
            'password' => 'Mật khẩu',
            'email' => 'Email'
        ];
    }
}
