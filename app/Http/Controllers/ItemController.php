<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function api_index(Request $request){
//        return Item::orderBy('id', 'DESC')->get();
        return Item::leftJoin('users', 'users.id', '=', 'items.user_id')->select('items.*', 'users.username')->orderBy('id', 'DESC')->get();
    }
    public function api_create(Request $request){
        Item::create([
            'name' => $request->name,
            'user_id' => Auth::guard('api')->id()
        ]);
        return true;
    }
    public function api_edit(Request $request){
        $item = Item::find($request->id);
        $item->name = $request->name;
        $item->save();
        return true;
    }
    public function api_destroy($id){
        Item::destroy($id);
        return true;
    }
}
