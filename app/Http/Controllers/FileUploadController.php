<?php

namespace App\Http\Controllers;

use App\Models\FileType;
use App\Models\FileUpload;
use App\Models\S3;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileUploadController extends Controller
{
    public function get_type()
    {
        return FileType::all();
    }

    public function size()
    {
        $sum_local = FileUpload::where('is_local', true)->sum('size');
        $sum_not_local = FileUpload::where('is_local', false)->sum('size');
        /*$all_files = FileUpload::select('size', 'is_local');
        $sum = 0;
        $sum_local = 0;
        $sum_not_local = 0;
        foreach ($all_files as $file){
            if ($file->is_local){
                $sum_local += $file->size;
            } else {
                $sum_not_local += $file->size;
            }
        }
        $sum = $sum_local + $sum_not_local;*/
        return ['local' => $sum_local, "not_local" => $sum_not_local, 'sum' => $sum_local + $sum_not_local];
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        if ($request->upload_option == 1){ // local
            if (((int)$this->size()['not_local'] + $request->file->getSize()) > 4831838208) {
                return ['status' => false, 'mess' => 'Hết mẹ dung lượng rồi bạn!'];
            } else {
                $mime_type = $request->file->getClientMimeType();
                $file_type = FileType::where('name', explode("/", $mime_type)[0])->first();
                if (!$file_type) {
                    $file_type = FileType::create([
                        'name' => explode("/", $mime_type)[0]
                    ]);
                }
                $file_name = Carbon::now()->timestamp . '-' . $request->file->getClientOriginalName();
                $file_name = preg_replace('/\s+/', '_', $file_name);
                $path = Storage::disk('local')->putFileAs($file_type->name, $request->file, $file_name, 'public');
                FileUpload::create([
                    'path' => "/" . $path,
                    'size' => $request->file->getSize(),
                    'file_type_id' => $file_type->id,
                    'user_id' => Auth::guard('api')->id(),
                    'is_local' => true
                ]);
                return ['status' => true];
            }
        } else{
            if (((int)$this->size()['local'] + $request->file->getSize()) > 4831838208) {
                return ['status' => false, 'mess' => 'Hết mẹ dung lượng rồi bạn!'];
            } else {
                $mime_type = $request->file->getClientMimeType();
                $file_type = FileType::where('name', explode("/", $mime_type)[0])->first();
                if (!$file_type) {
                    $file_type = FileType::create([
                        'name' => explode("/", $mime_type)[0]
                    ]);
                }
                $file_name = Carbon::now()->timestamp . '-' . $request->file->getClientOriginalName();
                $file_name = preg_replace('/\s+/', '_', $file_name);
                $path = Storage::disk('s3')->putFileAs($file_type->name, $request->file, $file_name, 'public');
                FileUpload::create([
                    'path' => env('AWS_BUCKET_URL') . $path,
                    'size' => $request->file->getSize(),
                    'file_type_id' => $file_type->id,
                    'user_id' => Auth::guard('api')->id(),
                    'is_local' => false
                ]);
                return ['status' => true];
            }
        }
    }


    public function show(FileUpload $fileUpload)
    {
        return FileUpload::join('file_types', 'file_types.id', 'file_uploads.file_type_id')->leftJoin('users', 'users.id', '=', 'file_uploads.user_id')->select('file_uploads.*', 'file_types.name as file_type_name', 'users.username')->orderBy('id', 'DESC')->get();
    }

    public function search(Request $request)
    {
        if ($request->type == -2){
            $list = FileUpload::where('is_local', false)->orderBy('id', 'DESC')->get();
        } elseif ($request->type == -1){
            $list = FileUpload::where('is_local', true)->orderBy('id', 'DESC')->get();
        } elseif ($request->type == 0){
            if ($request->name === null){
                $list = FileUpload::orderBy('id', 'DESC')->get(); //ok
            } else {
                $list = FileUpload::where('path', 'LIKE', '%'. $request->name .'%')->orderBy('id', 'DESC')->get();
            }
        } else {
            if ($request->name === null){
                $list = FileUpload::where('file_type_id', $request->type)->orderBy('id', 'DESC')->get();
            } else {
                $list = FileUpload::where('file_type_id', $request->type)->where('path', 'LIKE', '%'. $request->name .'%')->get();
            }
        }
        return $list;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileUpload $fileUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileUpload $fileUpload)
    {
        //
    }
}
