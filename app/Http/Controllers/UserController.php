<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddUserRequest;
use App\Jobs\SendMailJob;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    public function register(AddUserRequest $request){
        $hash = Str::uuid();
        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'hash' => $hash,
            'api_token' => Str::random(60),
        ]);
        $data['hash'] = $user->hash;
        $data['username'] = $user->username;
        SendMailJob::dispatch($request->email, $data);
        return true;
    }
    public function login(Request $request){
        if (Auth::guard('user')->attempt(['email' => $request->username, 'password' => $request->password]) || Auth::guard('user')->attempt(['username' => $request->username, 'password' => $request->password])){
            $new_token = Str::random(60);
            $user = Auth::guard('user')->user();
            $user->api_token = $new_token;
            $user->save();
            return ['status' => true, 'api_token' => $new_token];
        } else {
            return ['status' => false];
        }
    }
    public function user(Request $request){
        $user = Auth::guard('api')->user();
        return ['id' => $user->id, 'username' => $user->username, 'avatar' => $user->avatar, 'email' => $user->email, 'email_verified_at' => $user->email_verified_at];
    }
    public function logout(){
        $user = Auth::guard('api')->user();
        $user->api_token = null;
        $user->save();
        return true;
    }
    public function update(Request $request){
        $user = Auth::guard('api')->user();
        $user->username = $request->username;
        if ($request->new_pass != null){
            $user->password = bcrypt($request->new_pass);
            $user->save();
        }
        if ($user->email != $request->email){
            $user->email = $request->email;
            $user->email_verified_at = null;
            $data['hash'] = $user->hash;
            $data['username'] = $user->username;
            SendMailJob::dispatch($user->email, $data);
            $user->save();
            return ['status' => true, 're_verify_email' => true];
        }
        $user->save();
        return ['status' => true, 're_verify_email' => false];

    }
    public function change_avatar(Request $request){
        list($width, $height) = getimagesize($request->file);
        $imgResized = Image::make($request->file('file')->getRealPath())->resize(400, 400 / $width * $height);
        $file_name = Carbon::now()->timestamp . '-' . $request->file->getClientOriginalName();
        Storage::disk('local')->put('/avatar/' . $file_name, $imgResized->encode());
        $user = Auth::guard('api')->user();
        $user->avatar = '/avatar/' . $file_name;
        $user->save();
        return ['status' => true, 'avatar' => $user->avatar];
    }
    public function activeMail($hash){
        $user = User::where('hash', $hash)->first();
        if ($user->email_verified_at == null){
            if ($user) {
                $user->email_verified_at = Carbon::now();
                $user->save();
                $message = "Xác thực tài khoản email thành công.";
            } else{
                $message = "Đã có lỗi xảy ra.";
            }
        } else{
            $message = "Email của bạn đã được xác thực";
        }
        return view('resultActiveMail', compact('message'));
    }
    public function get_users(){
        return User::select('id', 'username', 'avatar')->where('id', '!=', Auth::guard('api')->id())->get();
    }
    public function resend_mail(){
        $user = Auth::guard('api')->user();
        $hash = Str::uuid();
        $user->hash = $hash;
        $user->save();
        $data['hash'] = $user->hash;
        $data['username'] = $user->username;
        SendMailJob::dispatch($user->email, $data);
        return true;
    }
}
