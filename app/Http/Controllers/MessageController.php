<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class MessageController extends Controller
{
    public function send(Request $request)
    {
        $mess = Message::create([
            'message' => $request->mess,
            'user_sender' => (int)Auth::guard('api')->id(),
            'user_receiver' => (int)$request->receiver
        ]);
        $url = "http://localhost:3000/send/" . $mess->id . "/" . $mess->user_sender . '/' . $mess->user_receiver;
        $req = Http::get($url);
        return $req->ok();
    }


    public function mess($id)
    {
        $user_id = Auth::guard('api')->id();
        $mess = Message::where('id', $id)->first();
        if ($mess->user_receiver == $user_id || $mess->user_sender == $user_id || $mess->user_receiver == 0){
            return $mess;
        } else{
            return null;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }
    public function show()
    {
        $users = User::select('id', 'username', 'avatar')->where('id', '!=', Auth::guard('api')->id())->get();
        $data = array();
        $current_id = Auth::guard('api')->id();
        for ($i = 0; $i<sizeof($users); $i++){
            $mess = Message::where('user_receiver', $current_id)->Where("user_sender", $users[$i]->id)->orWhere('user_receiver', $users[$i]->id)->where('user_sender', $current_id)->orderBy('id')->get();
            $data[$i] = ['user' => $users[$i], 'mess' => $mess];
        }
        $chat_all_mess = Message::where('user_receiver', 0)->join('users', 'users.id', 'messages.user_sender')->orderBy('id')->select('messages.*', 'users.username', 'users.avatar')->get();
        return ['data' => $data, 'chat_all_mess' => $chat_all_mess];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
