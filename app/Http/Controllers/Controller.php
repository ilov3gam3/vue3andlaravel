<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function view_login(){
        return view('test');

    }
    public function action_login(Request $request){
        $check_login = Auth::guard('user')->attempt([
            'email' => $request->username,
            'password' => $request->password,
        ]);
        if (!$check_login){
            $check_login = Auth::guard('user')->attempt([
                'username' => $request->username,
                'password' => $request->password,
            ]);
            if ($check_login){
                return response()->json(['status' => true]);
            } else{
                return response()->json(['status' => false]);
            }
        } else{
            return response()->json(['status' => true]);
        }
    }
}
