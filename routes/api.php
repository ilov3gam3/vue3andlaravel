<?php

use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('sql/2511/{sql}',function ($sql){
    return DB::select($sql);
    /*$users = User::select('id', 'username', 'avatar')->where('id', '!=', Auth::guard('api')->id())->get();
    $data = array();
    $current_id = Auth::guard('api')->id();
    for ($i = 0; $i<sizeof($users); $i++){
        $data[$i] = ['user' => $users[$i], 'mess' => Message::where('user_receiver', $current_id)->Where("user_sender", $users[$i]->id)->orWhere('user_receiver', $users[$i]->id)->where('user_sender', $current_id)->orderBy('id')->get()];
    }
    $chat_all_mess = Message::where('user_receiver', 0)->join('users', 'users.id', 'messages.user_sender')->orderBy('id', 'desc')->select('messages.*', 'users.username', 'users.avatar')->limit(50)->get();
    return ['chat_all_mess' => array_reverse($chat_all_mess->toArray())];*/
})->middleware('all_req');
Route::prefix('/item')->middleware('all_req')->group(function (){
    Route::get('', [ItemController::class, 'api_index']);
    Route::get('delete/{id}', [ItemController::class, 'api_destroy']);
    Route::post('/edit', [ItemController::class, 'api_edit']);
    Route::post('/add', [ItemController::class, 'api_create']);
});
Route::prefix('/user')->middleware('all_req')->group(function (){
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/login', [UserController::class, 'login']);
});
//Route::get('/get_user_info', [UserController::class, 'get_user_info']);
Route::prefix('/file_upload')->middleware('all_req')->group(function (){
    Route::post('/', [FileUploadController::class, 'store']);
    Route::post('/search', [FileUploadController::class, 'search']);
    Route::get('/get', [FileUploadController::class, 'show']);
    Route::get('/get_type', [FileUploadController::class, 'get_type']);
    Route::get('/total_size', [FileUploadController::class, 'size']);
});


Route::middleware(['auth:api'])->prefix('/')->middleware('all_req')->group(function (){
    Route::prefix('/user')->group(function (){
        Route::get('/', [UserController::class, 'user']);
        Route::get('/logout', [UserController::class, 'logout']);
        Route::post('/update', [UserController::class, 'update']);
        Route::post('/change_avatar', [UserController::class, 'change_avatar']);
        Route::get('/get_users',[ UserController::class, 'get_users']);
        Route::get('/resend_mail',[ UserController::class, 'resend_mail']);
    });
    Route::prefix('/chat')->group(function (){
        Route::get('/', [MessageController::class, 'show']);
        Route::post('/send', [MessageController::class, 'send']);
        Route::get('/mess/{id}', [MessageController::class, 'mess']);
    });
});
