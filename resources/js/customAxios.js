import axios from "axios";
const authAxios = axios.create({
    headers : {
        'Accept' : 'application/json',
    }
})
authAxios.interceptors.request.use((config) => {
    const token = localStorage.getItem('api_token')
    if (token){
        config.headers.Authorization = `Bearer ${token}`
    }
    return config
})
export default authAxios
