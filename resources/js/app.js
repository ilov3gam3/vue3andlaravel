import customAxios from "./customAxios";

require('./bootstrap');

import {createApp} from "vue";
import myApp from "./components/app.vue";
const app = createApp({})
app.component('myapp', myApp)
app.mount("#app")
