<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>{{$message}}</h1>
<p>Bạn sẽ được chuyển hướng đến trang chủ trong </p> <p id="countdown"></p>
<p>Hoặc là nhấn vô <a href="/">đây</a> để tới lun :v</p>
</body>
<script>
    window.onload = function() {
        window.setTimeout(function () {
            location.href = "{{env('app_url')}}";
        }, 10000);
        let timeLeft = 10;
        const x = setInterval(function() {
            document.getElementById("countdown").innerHTML = timeLeft;
            timeLeft--;
            if (timeLeft < 0) {
                clearInterval(x);
                document.getElementById("countdown").innerHTML = "0";
            }
        }, 1000);
    };
</script>
</html>
